import "../assets/css/challenge.css";
import Mobil from "../assets/images/img_car.png";
import Service from "../assets/images/img_service.png";
import Check from "../assets/images/check.png";
import Icon1 from "../assets/images/icon1.png";
import Icon2 from "../assets/images/icon2.png";
import Icon3 from "../assets/images/icon3.png";
import Icon4 from "../assets/images/icon4.png";
import People1 from "../assets/images/img_photo1.png";
import People2 from "../assets/images/img_photo2.png";
import People3 from "../assets/images/img_photo3.png";
import Star from "../assets/images/star.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";

function Home() {
  return (
    <>
      <div className="main">
        <div className="container konten1">
          <div className="row">
            <div className="col-lg-5">
              <h2>Sewa & Rental Mobil Terbaik di kawasan Palembang</h2>
              <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
              <a type="button" href="/cars" className="btn btn-success" id="btn-konten1">
                Mulai Sewa Mobil
              </a>
            </div>
            <div className="col-lg-7">
              <img src={Mobil} alt="gambar mobil" className="mobil" />
            </div>
          </div>
        </div>
      </div>

      <div className="ourservices" id="ourservices">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <img src={Service} alt="gambar servis" className="servis img-fluid" />
            </div>
            <div className="col-lg-6">
              <h4>Best Car Rental for any kind of trip in Palembang!</h4>
              <p>Sewa mobil di Palembang bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
              <ul className="list-group">
                <li>
                  <span>
                    <img src={Check} alt="check" />
                  </span>
                  Sewa Mobil Dengan Supir di Bali 12 Jam
                </li>
                <li>
                  <span>
                    <img src={Check} alt="check" />
                  </span>
                  Sewa Mobil Lepas Kunci di Bali 12 Jam
                </li>
                <li>
                  <span>
                    <img src={Check} alt="check" />
                  </span>
                  Sewa Mobil Jangka Panjang Bulanan
                </li>
                <li>
                  <span>
                    <img src={Check} alt="check" />
                  </span>
                  Gratis Antar - Jemput Mobil di Bandara
                </li>
                <li>
                  <span>
                    <img src={Check} alt="check" />
                  </span>
                  Layanan Airport Transfer / Drop In Out
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="whyus" id="whyus">
        <div className="container">
          <div className="title">
            <h4>Why Us?</h4>
            <p>Mengapa harus pilih Binar Car Rental?</p>
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-6">
              <div className="card">
                <div className="card-body">
                  <img src={Icon1} alt="icon_complete" />
                  <h5 className="card-title">Mobil Lengkap</h5>
                  <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-6">
              <div className="card">
                <div className="card-body">
                  <img src={Icon2} alt="icon_price" />
                  <h5 className="card-title">Harga Murah</h5>
                  <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-6">
              <div className="card">
                <div className="card-body">
                  <img src={Icon3} alt="icon_24hrs" />
                  <h5 className="card-title">Layanan 24 Jam</h5>
                  <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-6">
              <div className="card">
                <div className="card-body">
                  <img src={Icon4} alt="icon_professional" />
                  <h5 className="card-title">Sopir Profesional</h5>
                  <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="testimoni" id="testimoni">
        <center>
          <h4>Testimonial</h4>
          <p>Beberapa review positif dari pelanggan kami</p>
        </center>
        <div className="overflow">
          <div id="carouselExampleControlsNoTouching" className="carousel slide" data-bs-touch="false" data-bs-interval="false">
            <div className="carousel-inner">
              <center>
                <div className="carousel-item active">
                  <div className="row">
                    <div className="col-4">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-3">
                              <div className="img">
                                <img src={People1} alt="photo1" className="rounded-circle" />
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="star">
                                <img src={Star} alt="star1" />
                                <img src={Star} alt="star2" />
                                <img src={Star} alt="star3" />
                                <img src={Star} alt="star4" />
                                <img src={Star} alt="star5" />
                              </div>
                              <br />
                              <p>
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod”
                              </p>
                              <p>
                                <b>John Dee 32, Bromo</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-3">
                              <div className="img">
                                <img src={People2} alt="photo2" className="rounded-circle" />
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="star">
                                <img src={Star} alt="star1" />
                                <img src={Star} alt="star2" />
                                <img src={Star} alt="star3" />
                                <img src={Star} alt="star4" />
                                <img src={Star} alt="star5" />
                              </div>
                              <br />
                              <p>
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod”
                              </p>
                              <p>
                                <b>Fizi 6, Durian Runtuh</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-3">
                              <div className="img">
                                <img src={People3} alt="photo3" className="rounded-circle" />
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="star">
                                <img src={Star} alt="star1" />
                                <img src={Star} alt="star2" />
                                <img src={Star} alt="star3" />
                                <img src={Star} alt="star4" />
                                <img src={Star} alt="star5" />
                              </div>
                              <br />
                              <p>
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod”
                              </p>
                              <p>
                                <b>Ipin 6, Durian Runtuh</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="carousel-item">
                  <div className="row">
                    <div className="col-4">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-3">
                              <div className="img">
                                <img src={People2} alt="photo1" className="rounded-circle" />
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="star">
                                <img src={Star} alt="star1" />
                                <img src={Star} alt="star2" />
                                <img src={Star} alt="star3" />
                                <img src={Star} alt="star4" />
                                <img src={Star} alt="star5" />
                              </div>
                              <br />
                              <p>
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod”
                              </p>
                              <p>
                                <b>Fizi 6, Durian Runtuh</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-3">
                              <div className="img">
                                <img src={People3} alt="photo2" className="rounded-circle" />
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="star">
                                <img src={Star} alt="star1" />
                                <img src={Star} alt="star2" />
                                <img src={Star} alt="star3" />
                                <img src={Star} alt="star4" />
                                <img src={Star} alt="star5" />
                              </div>
                              <br />
                              <p>
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod”
                              </p>
                              <p>
                                <b>Ipin 6, Durian Runtuh</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-3">
                              <div className="img">
                                <img src={People1} alt="photo3" className="rounded-circle" />
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="star">
                                <img src={Star} alt="star1" />
                                <img src={Star} alt="star2" />
                                <img src={Star} alt="star3" />
                                <img src={Star} alt="star4" />
                                <img src={Star} alt="star5" />
                              </div>
                              <br />
                              <p>
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod”
                              </p>
                              <p>
                                <b>John Dee 32, Bromo</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="row">
                    <div className="col-4">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-3">
                              <div className="img">
                                <img src={People3} alt="photo1" className="rounded-circle" />
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="star">
                                <img src={Star} alt="star1" />
                                <img src={Star} alt="star2" />
                                <img src={Star} alt="star3" />
                                <img src={Star} alt="star4" />
                                <img src={Star} alt="star5" />
                              </div>
                              <br />
                              <p>
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod”
                              </p>
                              <p>
                                <b>Ipin 6, Durian Runtuh</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-3">
                              <div className="img">
                                <img src={People1} alt="photo2" className="rounded-circle" />
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="star">
                                <img src={Star} alt="star1" />
                                <img src={Star} alt="star2" />
                                <img src={Star} alt="star3" />
                                <img src={Star} alt="star4" />
                                <img src={Star} alt="star5" />
                              </div>
                              <br />
                              <p>
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod”
                              </p>
                              <p>
                                <b>John Dee 32, Bromo</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-3">
                              <div className="img">
                                <img src={People2} alt="photo3" className="rounded-circle" />
                              </div>
                            </div>
                            <div className="col-md-8">
                              <div className="star">
                                <img src={Star} alt="star1" />
                                <img src={Star} alt="star2" />
                                <img src={Star} alt="star3" />
                                <img src={Star} alt="star4" />
                                <img src={Star} alt="star5" />
                              </div>
                              <br />
                              <p>
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod”
                              </p>
                              <p>
                                <b>Fizi 6, Durian Runtuh</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </center>
            </div>
          </div>
        </div>
      </div>
      <div className="carousel-control">
        <button className="carousel-control__prev" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="prev">
          <span>
            <FontAwesomeIcon icon={faAngleLeft} />
          </span>
        </button>
        <button className="carousel-control__next" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="next">
          <span>
            <FontAwesomeIcon icon={faAngleRight} />
          </span>
        </button>
      </div>

      <div className="sewa_mobil" id="sewa_mobil">
        <div className="container">
          <div className="rectangle2">
            <div className="rectangle_content">
              <h2>Sewa Mobil di Palembang Sekarang</h2>
              <br />
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              <br />
              <a type="button" href="/cars" className="btn btn-success" id="sewa-btn">
                Mulai Sewa Mobil
              </a>
            </div>
          </div>
        </div>
      </div>

      <div className="faq" id="faq">
        <div className="container">
          <div className="row">
            <div className="col-md-5">
              <h4>Frequently Asked Question</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
            </div>
            <div className="col-md-7">
              <div className="accordion" id="accordionExample">
                <div className="accordion-item border border-1">
                  <h2 className="accordion-header" id="headingOne">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Apa saja syarat yang dibutuhkan?
                    </button>
                  </h2>
                  <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel odit rerum voluptatum, laboriosam animi laborum veniam vero, enim sapiente commodi excepturi. Laborum accusantium nulla suscipit. Esse sequi quasi ipsa
                        iste?
                      </p>
                    </div>
                  </div>
                </div>
                <div className="accordion-item border border-1 mt-3">
                  <h2 className="accordion-header" id="headingTwo">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Berapa hari minimal sewa mobil lepas kunci?
                    </button>
                  </h2>
                  <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vero voluptatibus excepturi quo aspernatur beatae culpa maiores officiis amet laboriosam aperiam fugiat et eum eaque repudiandae animi illum quia, nobis
                        corrupti?
                      </p>
                    </div>
                  </div>
                </div>
                <div className="accordion-item border border-1 mt-3">
                  <h2 className="accordion-header" id="headingThree">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Berapa hari sebelumnya sabaiknya booking sewa mobil?
                    </button>
                  </h2>
                  <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat officia tempora voluptate ex, rem enim eos adipisci, explicabo doloribus obcaecati vero quod reprehenderit, odit ipsa? Aliquam quibusdam dignissimos
                        mollitia alias!
                      </p>
                    </div>
                  </div>
                </div>
                <div className="accordion-item border border-1 mt-3">
                  <h2 className="accordion-header" id="headingFour">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                      Apakah Ada biaya antar-jemput?
                    </button>
                  </h2>
                  <div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat officia tempora voluptate ex, rem enim eos adipisci, explicabo doloribus obcaecati vero quod reprehenderit, odit ipsa? Aliquam quibusdam dignissimos
                        mollitia alias!
                      </p>
                    </div>
                  </div>
                </div>
                <div className="accordion-item border border-1 mt-3">
                  <h2 className="accordion-header" id="headingFive">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                      Bagaimana jika terjadi kecelakaan
                    </button>
                  </h2>
                  <div id="collapseFive" className="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat officia tempora voluptate ex, rem enim eos adipisci, explicabo doloribus obcaecati vero quod reprehenderit, odit ipsa? Aliquam quibusdam dignissimos
                        mollitia alias!
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
