import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchCars, getAllCars } from "../features/cars/carsSlice";
import "../assets/css/challenge.css";
import "../assets/css/cari.css";
import Mobil from "../assets/images/img_car.png";
import IconUser from "../assets/images/fi_users.png";
import IconSetting from "../assets/images/fi_settings.png";
import IconCalender from "../assets/images/fi_calendar.png";

function Cari() {
  const [carsFilter, setCarsFilter] = useState([]);
  const [driver, setDriver] = useState();
  const [tanggal, setTanggal] = useState();
  const [waktu, setWaktu] = useState();
  const [capacity, setCapacity] = useState();

  const dispatch = useDispatch();
  const cars = useSelector(getAllCars);
  const carsStatus = useSelector((state) => state.cars.status);
  const error = useSelector((state) => state.cars.error);

  const filterCars = () => {
    const filteredCars = cars.filter((item) => {
      var tipeDriver = null;
      if (driver === "Dengan Sopir") {
        tipeDriver = true;
      } else if (driver === "Tanpa Sopir") {
        tipeDriver = false;
      }
      const driverFilter = item.available === tipeDriver;
      const dateTime = new Date(`${tanggal} ${waktu.substr(0, 5)}`);
      const beforeEpochTime = dateTime.getTime();
      const itemDate = new Date(item.availableAt);
      const dateFilter = itemDate.getTime() < beforeEpochTime;
      const capacityFilter = item.capacity >= capacity;
      console.log(dateFilter);

      return driverFilter && dateFilter && capacityFilter;
    });
    setCarsFilter(filteredCars);
  };

  useEffect(() => {
    if (carsStatus === "idle") {
      dispatch(fetchCars());
    }
  }, [carsStatus, dispatch]);

  let content;

  if (carsStatus === "loading") {
    content = <div>Loading...</div>;
  } else if (carsStatus === "succeeded") {
    content = (
      <>
        <div className="main">
          <div className="container konten1">
            <div className="row" id="konten1">
              <div className="col-lg-5">
                <h2>Sewa & Rental Mobil Terbaik di kawasan Palembang</h2>
                <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                <br />
              </div>
              <div className="col-lg-7">
                <img src={Mobil} alt="gambar mobil" className="mobil" />
              </div>
            </div>
            <div className="floating-form" id="floating-form">
              <div className="container">
                <div className="card shadow-sm p-2">
                  <div className="card-body">
                    <div className="form-display">
                      <div className="form-field">
                        <label for="driver" className="form-label">
                          Tipe Driver
                        </label>
                        <select className="form-select" name="driver" id="driver" onChange={(e) => setDriver(e.target.value)}>
                          <option value="" disabled selected hidden>
                            Pilih Tipe Driver
                          </option>
                          <option value="Dengan Sopir">Dengan Sopir</option>
                          <option value="Tanpa Sopir">Tanpa Sopir (Lepas Kunci)</option>
                        </select>
                      </div>
                      <div className="form-field">
                        <label for="date" className="form-label">
                          Tanggal
                        </label>
                        <input type="date" placeholder="Pilih Tanggal" className="form-control calendar-label" name="date" id="date" onChange={(e) => setTanggal(e.target.value)} />
                      </div>
                      <div className="form-field">
                        <label for="time" className="form-label">
                          Waktu Jemput/Ambil
                        </label>
                        <select className="form-select clock-label" name="time" id="time" onChange={(e) => setWaktu(e.target.value)}>
                          <option value="" disabled selected hidden>
                            Pilih Waktu
                          </option>
                          <option value="08:00 WIB">08.00 WIB</option>
                          <option value="09:00 WIB">09.00 WIB</option>
                          <option value="10:00 WIB">10.00 WIB</option>
                          <option value="11:00 WIB">11.00 WIB</option>
                          <option value="12:00 WIB">12.00 WIB</option>
                        </select>
                      </div>
                      <div className="form-field">
                        <label for="penumpang" className="form-label">
                          Jumlah Penumpang (optional)
                        </label>
                        <input type="text" className="form-control user-label" name="penumpang" id="penumpang" placeholder="Jumlah Penumpang" onChange={(e) => setCapacity(Number(e.target.value))} />
                      </div>
                      <div className="form-field">
                        <button className="btn btn-success" id="load-btn" onClick={filterCars}>
                          Cari Mobil
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container" id="list-mobil">
          <div id="cars-container" className="row">
            {carsFilter.map((item) => {
              return (
                <div className="col-lg-4 col-md-6 col-sm-12 cars-body">
                  <div className="card card-mobil">
                    <img src={item.image.replace("./", "https://res.cloudinary.com/dyhrsgr2m/image/upload/v1653577874/")} className="card-img-top" alt={item.manufacture} />
                    <div className="card-body">
                      <p className="card-text pertama">
                        {item.manufacture} {item.model} / {item.type}
                      </p>
                      <p className="card-title">Rp {item.rentPerDay} / hari</p>
                      <p className="card-text">{item.description.slice(0, 35)}</p>
                      <p className="card-text">
                        <img src={IconUser} alt="" />
                        {item.capacity} Penumpang
                      </p>
                      <p className="card-text">
                        <img src={IconSetting} alt="" />
                        {item.transmission}
                      </p>
                      <p className="card-text">
                        <img src={IconCalender} alt="" />
                        {item.year}
                      </p>
                      <a href="#" className="btn btn-success">
                        Pilih Mobil
                      </a>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </>
    );
  } else if (carsStatus === "failed") {
    content = <div>{error}</div>;
  }

  return <>{content}</>;
}

export default Cari;
