import "../assets/css/challenge.css";

function Navbar() {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <a className="navbar-brand" href="/">
            <div className="rectangle"></div>
          </a>
          <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="offcanvas offcanvas-end" tabIndex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
            <div className="offcanvas-header">
              <p id="offcanvasRightLabel">
                <b>BCR</b>
              </p>
              <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div className="offcanvas-body">
              <div className="navbar-nav ms-auto">
                <a className="nav-link" href="#ourservices">
                  Our Services
                </a>
                <a className="nav-link" href="#whyus">
                  Why Us
                </a>
                <a className="nav-link" href="#testimoni">
                  Testimonial
                </a>
                <a className="nav-link" href="#faq">
                  FAQ
                </a>
                <button type="button" className="btn btn-success">
                  Register
                </button>
              </div>
            </div>
          </div>
        </div>
      </nav>
      ;
    </>
  );
}

export default Navbar;
