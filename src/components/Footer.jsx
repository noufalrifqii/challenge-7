import "../assets/css/challenge.css";
import Facebook from "../assets/images/icon_facebook.png";
import Instagram from "../assets/images/icon_instagram.png";
import Twitter from "../assets/images/icon_twitter.png";
import Mail from "../assets/images/icon_mail.png";
import Twitch from "../assets/images/icon_twitch.png";

function Footer() {
  return (
    <>
      <div className="footer" id="footer">
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-lg-3">
              <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
              <p>binarcarrental@gmail.com</p>
              <p>081-233-334-808</p>
            </div>
            <div className="col-md-2 col-lg-2 bold">
              <p>Our services</p>
              <p>Why Us</p>
              <p>Testimonial</p>
              <p>FAQ</p>
            </div>
            <div className="col-md-4 col-lg-3">
              <p>Connect with us</p>
              <img src={Facebook} alt="icon_facebook" />
              <img src={Instagram} alt="icon_instagram" />
              <img src={Twitter} alt="icon_twitter" />
              <img src={Mail} alt="icon_mail" />
              <img src={Twitch} alt="icon_twitch" />
            </div>
            <div className="col-md-2 col-lg-4">
              <p>Copyright Binar 2022</p>
              <div className="rectangle"></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Footer;
