# Binar Car Rental With React - Chapter 7 - Muhammad Noufal Rifqi Iman

Sebuah website Binar Car Rental yang dibangun dalam bentuk React JS.

## Deskripsi

Project ini merupakan lanjutan dari project challenge ke-4. Di mana project ini dibangun dalam bentuk React JS. Project ini juga menggunakan bootstrap serta redux toolkit dan react redux.

## Persyaratan

Sebelum masuk ke proses instalasi dan penggunaan, diperlukan untuk melakukan instalasi beberapa kebutuhan seperti di bawah ini :

- [Node.js](https://nodejs.org/)
- [NPM (Package Manager)](https://www.npmjs.com/)

## Instalasi dan Penggunaan

- Clone repository ini <br />
  ```bash
  git clone https://gitlab.com/noufalrifqii/noufal-car-management-dashboard
  ```
- Download semua package yang dibutuhkan dan depedenciesnya <br />
  ```bash
  npm install
  ```
- Jalankan Server
  ```bash
  npm start
  ```
- Website akan otomatis terbuka pada [http://localhost:3000](http://localhost:3000) di browser

## Tech Stack

- Node.js 16.14 (Runtime Environment)
- ReactJS 18.1.0 (UI Library)
- Redux Toolkit 1.8.2 (State Management)
- React Redux 8.0.2 (State Management)
